<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use Illuminate\Http\Request;

class ModeratorController extends Controller
{
    public function __construct()
    {
        $this->middleware('role');
    }

    public function index()
    {
        $ads = Ad::notModerated();
        return view('Ad.moderator_home', compact('ads'));
    }

    public function enable($id)
    {
        Ad::enable($id, auth()->user()->id);
        return redirect()
            ->route('moderator_home')
            ->with('status', 'Объявление размещено');
    }
}
