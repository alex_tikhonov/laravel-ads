<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ad;

class AdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add()
    {
        return view('Ad.add');
    }

    public function store(Request $request)
    {
        $request->merge(['user_id' => auth()->user()->id]);
        $post = new Ad($request->all());
        $post->save();
        return redirect()
            ->route('home')
            ->with('status', 'Новый пост успешно создан');
    }
}
