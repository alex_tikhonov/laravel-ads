<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'user_id',
    ];

    static public function notModerated()
    {
        return self::all()->where('moderated', 0);
    }

    static public function moderated()
    {
        return self::all()->where('moderated', 1);
    }

    static public function enable($id, $moderator_id)
    {
        $ad = Ad::find($id);
        $ad->moderated = 1;
        $ad->moderator_id = $moderator_id;
        $ad->save();
    }

    /**
     * Связь модели Ad с моделью User, позволяет получить
     * автора поста
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
