<?php

namespace App\Traits;

use App\Models\Role;

trait HasRoles
{
    /**
     * Связь модели User с моделью Role, позволяет получить все роли пользователя
     */
    public function roles()
    {
        return $this
            ->belongsToMany(Role::class, 'user_role')
            ->withTimestamps();
    }

    /**
     * Имеет текущий пользователь роль $role?
     */
    public function hasRole($role) {
        return $this->roles->contains('role', $role);
    }

    /**
     * Возвращает массив всех ролей текущего пользователя
     */
    public function getAllRoles() {
        return $this->roles->pluck('role')->toArray();
    }

    /**
     * Добавить текущему пользователю роли $roles
     * (в дополнение к тем, что уже были назначены)
     */
    public function assignRoles(...$roles) {
        $roles = Role::whereIn('role', $roles)->get();
        if ($roles->count() === 0) {
            return $this;
        }
        $this->roles()->syncWithoutDetaching($roles);
        return $this;
    }

    /**
     * Отнять у текущего пользователя роли $roles
     * (из числа тех, что были назначены ранее)
     */
    public function unassignRoles(...$roles) {
        $roles = Role::whereIn('role', $roles)->get();
        if ($roles->count() === 0) {
            return $this;
        }
        $this->roles()->detach($roles);
        return $this;
    }

    /**
     * Назначить текущему пользователю роли $roles
     * (отнять при этом все ранее назначенные роли)
     */
    public function refreshRoles(...$roles) {
        $roles = Role::whereIn('role', $roles)->get();
        if ($roles->count() === 0) {
            return $this;
        }
        $this->roles()->sync($roles);
        return $this;
    }
}
