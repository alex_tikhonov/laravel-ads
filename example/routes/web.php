<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/add', [App\Http\Controllers\AdController::class, 'add'])->name('add');
Route::post('/add', [App\Http\Controllers\AdController::class, 'store'])->name('store');
Route::get('/moderator', [App\Http\Controllers\ModeratorController::class, 'index'])->name('moderator_home');
Route::get('/ad/enable/{id}', [App\Http\Controllers\ModeratorController::class, 'enable'])->name('enable');
